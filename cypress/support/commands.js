
  Cypress.Commands.add('compareJSONFilesAndAssert', (file1Path, file2Path) => {
    cy.readFile(file1Path).then((file1PathJSON) => {
      cy.readFile(file2Path).then((file2PathJSON) => {
        // Compare JSON objects manually
        const differences = compareObjects(file1PathJSON, file2PathJSON);

        if (differences.length > 0) {
          // assert differences     
          differences.forEach((diff) => {
            expect(diff.actual, `Failed assertion at path: ${diff.path}`).to.deep.equal(diff.expected);
          }); 
  
        } else {
          expect(differences, 'No differences found.').to.deep.equal([]);
         
        }
      });
    });
  });
 
  
  // Helper function to compare objects manually
  function compareObjects(obj1, obj2, path = '') {
    const differences = [];
  
    for (const key in obj1) {
      const currentPath = path ? `${path}.${key}` : key;
  
      if (typeof obj1[key] === 'object' && typeof obj2[key] === 'object') {
        differences.push(...compareObjects(obj1[key], obj2[key], currentPath));
      } else if (obj1[key] !== obj2[key]) {
        differences.push({
          path: currentPath,
          expected: obj1[key],
          actual: obj2[key],
        });
      }
    }
  
    return differences;
  }
  
  
