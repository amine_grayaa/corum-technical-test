/// <reference types="cypress"/>



describe('JSON Comparison Test', () => {
  it('should compare two JSON files and log all differences in the assertion', () => {
    const file1Path = 'cypress/e2e/first_json_file.json';
    const file2Path = 'cypress/e2e/second_json_file.json';  

    // Use the custom command to compare and assert JSON files
    cy.compareJSONFilesAndAssert(file1Path, file2Path);
  });
});














